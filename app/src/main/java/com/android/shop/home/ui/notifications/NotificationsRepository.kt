package com.android.shop.home.ui.notifications

class NotificationsRepository {

    companion object {
        fun getInstance(): NotificationsRepository {
            val mInstance: NotificationsRepository by lazy { NotificationsRepository() }
            return mInstance
        }
    }
}