package com.android.shop.home.data.model

data class Data(
    val viewList: List<ViewList>,
    val viewType: String,
    val label: String
)