package com.android.shop.home.data.model

data class ViewList(
    val catName: String,
    val imgUrl: String
)