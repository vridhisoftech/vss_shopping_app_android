package com.android.shop.home.data.model

data class Response(
    val `data`: List<Data>,
    val message: String,
    val status: Int
)