package com.android.shop.home.ui.discover


class DiscoverRepository {

    companion object {
        fun getInstance(): DiscoverRepository {
            val mInstance: DiscoverRepository by lazy { DiscoverRepository() }
            return mInstance
        }
    }
}