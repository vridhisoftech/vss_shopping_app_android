package com.android.shop.home.ui.home.newsfeed

data class FeedResponse(
    val data: List<Data>
)